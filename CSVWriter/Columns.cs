﻿//
//  Copyright 2014  Alexander Samolov
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

using System;
using System.Collections.Generic;

namespace CSVWriter
{
	/// <summary>
	/// Descriptor of columns inside CSV file.
	/// Each CSV file consist of number of columns, with column having a name and formatter.
	/// <term>Formatter</term>
	/// The formatter is a delegate of type <see cref="System.Func`2"/> which shall return a string on the provided object.
	/// <h1>Order of columns</h1>
	/// Columns are written to the file in the order, in which they are added.
	/// <h1>Error Handling</h1>
	/// <code>null</code> value returned by formatter threaten as empty string.
	/// If formatter crashes with an exception - the whole process is aborted and you shall care of cleaning up the resources.
	/// The excepion is throwed back wrapped inside <see cref="CSVWriter.CSVWriterException"/>.
	/// </summary>
	public class Columns<T> : IEnumerable<Column<T>>
    {
		private readonly List<Column<T>> _cols;

		/// <summary>
		/// Initializes a new instance of the <see cref="CSVWriter.Columns`1"/> class.
		/// </summary>
		public Columns()
		{
			_cols = new List<Column<T>>();
		}

		/// <summary>
		/// Defensive copy constructor
		/// </summary>
		/// <param name="other">columns to copy</param>
		public Columns(Columns<T> other) : this()
		{
			if (other == null) {
				throw new ArgumentNullException("other");
			}
			_cols.AddRange(other);
		}

        /// <summary>
        /// Add the <see cref="CSVWriter.Column`1"/> with a specified name and formatter.
        /// </summary>
        /// <param name="name">Name of column.</param>
        /// <param name="formatter">Column formatter</param>
        public Columns<T> Add(string name, Func<T, string> formatter)
        {
			if (string.IsNullOrWhiteSpace(name)) {
				throw new ArgumentNullException("name", "Column shall not be null or empty");
			}

			if (formatter == null) {
				throw new ArgumentNullException("formatter", "Please provide formatter for the object " + typeof(T));
			}

			_cols.Add(new Column<T>(name, formatter));

            return this;
        }

		/// <summary>
		/// Count of columns in the collection
		/// </summary>
		/// <value>Columns count.</value>
		public int Count { get { return _cols.Count; } }

		#region IEnumerable implementation

		/// <summary>
		/// Gets the enumerator.
		/// </summary>
		/// <returns>The enumerator.</returns>
		public IEnumerator<Column<T>> GetEnumerator()
		{
			return _cols.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		#endregion
    }
}
