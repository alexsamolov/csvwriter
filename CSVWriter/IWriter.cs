﻿//
//  Copyright 2014  Alexander Samolov
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

using System;
using System.Collections.Generic;

namespace CSVWriter
{
	/// <summary>
	/// Writer of CSV format. Writes a single item. The column names and what to write are dictated by <see cref="CSVWriter.Columns`1"/>
	/// Writer shall be created by factory, <see cref="CSVWriter.WriterFactory`1"/>.
	/// </summary>
	/// <typeparam name="T">Type of data to write to CSV</typeparam>
    public interface IWriter<T>
	{
		/// <summary>
		/// Write the row with specified item.
		/// The header row is written on first time call
		/// </summary>
		/// <param name="item">Item.</param>
        void Write(T item);

		/// <summary>
		/// Write the specified list of items. Items are written in the specified order
		/// </summary>
		/// <param name="data">List of items to write</param>
        void Write(IEnumerable<T> data);
    }
}
