﻿//
//  Copyright 2014  Alexander Samolov
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CSVWriter
{
    class Writer<T> : IWriter<T>
    {
		private const char DELIM = ',';
		private readonly char[] QOUTED = { DELIM, '"' };

		private const string QUOTE = "\"";
		private const string DBL_QUOTE = "\"\"";

		private readonly Columns<T> _cols;
		private readonly TextWriter _stream;

		private bool headerRowWriten = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="CSVWriter.Writer`1"/> class.
		/// </summary>
		/// <param name="cols">Columns definition</param>
		/// <param name="stream">Stream to write output</param>
		public Writer(Columns<T> cols, TextWriter stream)
        {
			if (cols == null) {
				throw new ArgumentNullException("cols");
			}

			if (cols.Count == 0) {
				throw new ArgumentException("cols shall have at least one column!", "cols");
			}
			// creates a defensive copy
			this._cols = new Columns<T>(cols);

			if (stream == null) {
				throw new ArgumentNullException("stream");
			}
			_stream = stream;
        }

		/// <summary>
		/// Write the row with specified item.
		/// The header row is written on first time call
		/// </summary>
		/// <param name="item">Item.</param>
        public virtual void Write(T item)
        {
			if (item == null) {
				throw new ArgumentNullException("item");
			}

			if (!headerRowWriten) {
				WriteHeaderRow();
				headerRowWriten = true;
			}

			int colsToWrite = _cols.Count;

			foreach (var col in _cols) {
				string value;
				try {
					value = col.Formatter(item);
				} catch (Exception e) {
					var msg = string.Format("Column: {0}, failed to run formatter. Object: {1}", col.Name, item);
					throw new CSVWriterException(msg, e);
				}

				if (value == null) {
					value = "";
				}
				if (ShallQuote(value)) {
					value = Quote(value);
				}

				colsToWrite--;
				try {
					_stream.Write(value);
					// if there is no more columns - write a new line
					if (colsToWrite == 0) {
						_stream.WriteLine();
					} else {
						// if there are columns - write a delimiter
						_stream.Write(DELIM);
					}
				} catch (IOException e) {
					throw new CSVWriterException("Error while writing to output stream", e);
				} catch (ObjectDisposedException e) {
					throw new CSVWriterException("Error while writing to output stream: stream is closed!", e);
				}
			}
        }

		/// <summary>
		/// Write the specified list of items. Items are written in the specified order
		/// </summary>
		/// <param name="data">List of items to write</param>
        public virtual void Write(IEnumerable<T> data)
		{
			if (data == null) {
				throw new ArgumentNullException("data");
			}

			foreach (var item in data) {
				Write(item);
			}
        }

		/// <summary>
		/// Write Single header row
		/// </summary>
		private void WriteHeaderRow() {
			int colsToWrite = _cols.Count;

			foreach (var col in _cols) {
				string name;
				name = col.Name;

				if (name == null) {
					name = "";
				}
				if (ShallQuote(name)) {
					name = Quote(name);
				}

				colsToWrite--;
				try {
					_stream.Write(name);
					// if there is no more columns - write a new line
					if (colsToWrite == 0) {
						_stream.WriteLine();
					} else {
						// if there are columns - write a delimiter
						_stream.Write(DELIM);
					}
				} catch (IOException e) {
					throw new CSVWriterException("Error while writing to output stream", e);
				} catch (ObjectDisposedException e) {
					throw new CSVWriterException("Error while writing to output stream: stream is closed!", e);
				}
			}
		}

		private bool ShallQuote(string value)
		{
			if (value.IndexOfAny(QOUTED) > 0) {
				return true;
			}
			return false;
		}

		private string Quote(string value)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(QUOTE)
				.Append(value.Replace(QUOTE, DBL_QUOTE))
				.Append(QUOTE);
			return sb.ToString();
		}
    }

    public class WriterFactory
    {
        /// <summary>
		/// Create the CSV Writer. Writer does nothing to the unrderlying stream except of writing data. You shall manage the stream (close, flush) externally.
        /// </summary>
        /// <param name="cols">Columns description</param>
        /// <param name="output">Output stream</param>
        /// <typeparam name="T">Type of data to write to CSV</typeparam>
        public static IWriter<T> Create<T>(Columns<T> cols, TextWriter output)
        {
			return new Writer<T>(cols, output);
        }
    }
}
