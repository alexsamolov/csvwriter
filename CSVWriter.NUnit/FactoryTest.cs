﻿//
//  Copyright 2014  Alexander Samolov
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using NUnit.Framework;
using System;
using System.IO;

namespace CSVWriter.NUnit
{
	[TestFixture]
	public class FactoryTest
	{
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullColumns()
		{
			Columns<DataItem> cols = null;
			using (var stream = new StringWriter()) {
				var writer = WriterFactory.Create(cols, stream);
				// just to use the pointer
				Assert.IsNull(writer);
			}
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void EmptyColumns()
		{
			var cols = new Columns<DataItem>();
			using (var stream = new StringWriter()) {
				var writer = WriterFactory.Create(cols, stream);
				// just to use the pointer
				Assert.IsNull(writer);
			}
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullStream()
		{
			var cols = ColumnsTest.CreateCols();
			var writer = WriterFactory.Create(cols, null);
			// just to use the pointer
			Assert.IsNull(writer);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullBoth()
		{
			Columns<DataItem> cols = null;
			var writer = WriterFactory.Create(cols, null);
			// just to use the pointer
			Assert.IsNull(writer);
		}

		[Test]
		public void Creation()
		{
			var cols = ColumnsTest.CreateCols();
			using (var stream = new StringWriter()) {
				var writer = WriterFactory.Create(cols, stream);
				Assert.IsNotNull(writer);
			}
		}
	}
}

