﻿//
//  Copyright 2014  Alexander Samolov
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using NUnit.Framework;
using System;
using System.IO;
using System.Collections.Generic;

namespace CSVWriter.NUnit
{
	[TestFixture]
	public class WriterTest
	{
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullItemTest()
		{
			var cols = ColumnsTest.CreateCols();
			using (var stream = new StringWriter()) {
				var writer = WriterFactory.Create(cols, stream);

				DataItem item = null;
				writer.Write(item);
			}
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullCollectionTest()
		{
			var cols = ColumnsTest.CreateCols();
			using (var stream = new StringWriter()) {
				var writer = WriterFactory.Create(cols, stream);

				List<DataItem> items = null;

				writer.Write(items);
			}
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullItemsInCollectionTest()
		{
			var cols = ColumnsTest.CreateCols();
			using (var stream = new StringWriter()) {
				var writer = WriterFactory.Create(cols, stream);

				List<DataItem> items = new List<DataItem>();
				DataItem item = new DataItem { Name = "Z", Amount=1000, DOB=new DateTime(2014, 01, 01), Phone = "+12013452398" };

				items.Add(item);
				items.Add(null);

				writer.Write(items);
			}
		}

		[Test]
		public void NullValueTest()
		{
			var expected = 
				@"Name,Amount,Phone
,5.50,
Kate,10.00,+79049999999
,5.50,
Z,10.00,+79049999999
";
			var items = new DataItem[] {
				new DataItem { Name = null, Amount=5.5m, DOB=new DateTime(1986, 07, 28), Phone=null },
				new DataItem { Name = "Kate", Amount=10, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" },
				new DataItem { Name = null, Amount=5.5m, DOB=new DateTime(1986, 07, 28), Phone=null },
				new DataItem { Name = "Z", Amount=10, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" }
			};

			var cols = new Columns<DataItem>();
			cols.Add("Name", x => x.Name);
			cols.Add("Amount", x => x.Amount.ToString("N"));
			cols.Add("Phone", x => x.Phone);

			Assert.AreEqual(expected, Serialize(cols, items));
		}

		[Test]
		public void OneColumnTest() {
			var expected = 
				@"Name
Alex
Kate
Z
";
			var items = new DataItem[] {
				new DataItem { Name = "Alex", Amount=5.5m, DOB=new DateTime(1986, 07, 28), Phone="+79049999999" },
				new DataItem { Name = "Kate", Amount=10, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" },
				new DataItem { Name = "Z", Amount=10, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" }
			};

			var cols = new Columns<DataItem>();
			cols.Add("Name", x => x.Name);

			Assert.AreEqual(expected, Serialize(cols, items));
		}

		[Test]
		public void CollectionVsOneByOneTest()
		{
			var expected = 
				@"Name,Amount,Date of Birth,Phone
Alex,5.50,28-07-1986,+79049999999
Kate,10.00,29-03-1988,+79049999999
Z,""1,000.00"",29-03-1988,+79049999999
";
			var items = new DataItem[] {
				new DataItem { Name = "Alex", Amount=5.5m, DOB=new DateTime(1986, 07, 28), Phone="+79049999999" },
				new DataItem { Name = "Kate", Amount=10, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" },
				new DataItem { Name = "Z", Amount=1000, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" }
			};

			var cols = ColumnsTest.CreateCols();
			var oneByOne = "";

			using(var output = new StringWriter()) {
				var w = WriterFactory.Create(cols, output);
				foreach (var item in items) {
					w.Write(item);
				}
				oneByOne = output.ToString();
			}

			var collection = Serialize(cols, items);

			Assert.AreEqual(expected, collection);
			Assert.AreEqual(expected, oneByOne);
			Assert.AreEqual(oneByOne, collection);
		}

		[Test]
		public void EscapeQuoteTest()
		{
			var cols = new Columns<DataItem>();
			cols.Add("Name \"Super\"", x => x.Name);
			cols.Add("Phone", x => x.Phone);

			var expected=
@"""Name """"Super"""""",Phone
""Alex """"Z"""""",""123""""4""""""
Z,123
";
			var items = new DataItem[] {
				new DataItem { Name = "Alex \"Z\"", Amount=5.5m, DOB=new DateTime(1986, 07, 28), Phone="123\"4\"" },
				new DataItem { Name = "Z", Amount=1000, DOB=new DateTime(1988, 03, 29), Phone="123" }
			};


			Assert.AreEqual(expected, Serialize(cols, items));
		}


		[Test]
		public void EscapeCommaTest()
		{
			var cols = new Columns<DataItem>();
			cols.Add("Name,Super", x => x.Name);
			cols.Add("Phone", x => x.Phone);

			var expected=
@"""Name,Super"",Phone
""Alex, Z"",""123,4""
Z,123
";
			var items = new DataItem[] {
				new DataItem { Name = "Alex, Z", Amount=5.5m, DOB=new DateTime(1986, 07, 28), Phone="123,4" },
				new DataItem { Name = "Z", Amount=1000, DOB=new DateTime(1988, 03, 29), Phone="123" }
			};


			Assert.AreEqual(expected, Serialize(cols, items));
		}

		[Test]
		public void NoWritesTest()
		{
			var expected = "";
			var cols = ColumnsTest.CreateCols();
			var items = new List<DataItem>();

			Assert.AreEqual(expected, Serialize(cols, items));
		}

		[Test]
		[ExpectedException(typeof(CSVWriterException))]
		public void ClosedStreamTest()
		{
			var items = new DataItem[] {
				new DataItem { Name = "Alex", Amount=5.5m, DOB=new DateTime(1986, 07, 28), Phone="+79049999999" },
				new DataItem { Name = "Kate", Amount=10, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" },
				new DataItem { Name = "Z", Amount=1000, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" }
			};

			var cols = ColumnsTest.CreateCols();

			var output = new StringWriter();
			output.Close();

			var w = WriterFactory.Create(cols, output);
			foreach (var item in items) {
				w.Write(item);
			}
		}

		[Test]
		[ExpectedException(typeof(CSVWriterException))]
		public void FailedSerializerTest()
		{
			var items = new DataItem[] {
				new DataItem { Name = null, Amount=5.5m, DOB=new DateTime(1986, 07, 28), Phone=null },
				new DataItem { Name = "Kate", Amount=10, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" },
				new DataItem { Name = null, Amount=5.5m, DOB=new DateTime(1986, 07, 28), Phone=null },
				new DataItem { Name = "Z", Amount=10, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" }
			};

			var cols = new Columns<DataItem>();
			cols.Add("Name", x => x.Name);
			cols.Add("Amount", x => x.Amount.ToString("D"));
			cols.Add("Phone", x => x.Phone);

			Serialize(cols, items);
		}

		static string Serialize(Columns<DataItem> cols, IEnumerable<DataItem> items) {
			using(var output = new StringWriter()) {
				var w = WriterFactory.Create(cols, output);
				w.Write(items);
				return output.ToString();
			}
		}
	}
}
