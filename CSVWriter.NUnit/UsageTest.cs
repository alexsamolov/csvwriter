﻿//
//  Copyright 2014  Alexander Samolov
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;

namespace CSVWriter.NUnit
{
	[TestFixture]
	public class UsageTest
	{
		[Test]
		public void FullUsage()
		{
			var expected = 
@"Name,Amount,Date of Birth,Phone
Z,""1,000.00"",01-01-2014,+12013452398
Alex,5.50,28-07-1986,+79049999999
Kate,10.00,29-03-1988,+79049999999
";
			var cols = new Columns<DataItem>();
			cols.Add("Name", x => x.Name)
				.Add("Amount", x => x.Amount.ToString("N"))
				.Add("Date of Birth", x => x.DOB.ToString("dd-MM-yyyy"))
				.Add("Phone", x => x.Phone);

			IEnumerable<DataItem> data = new DataItem[] {
				new DataItem { Name = "Alex", Amount=5.5m, DOB=new DateTime(1986, 07, 28), Phone="+79049999999" },
				new DataItem { Name = "Kate", Amount=10, DOB=new DateTime(1988, 03, 29), Phone="+79049999999" }
			};

			DataItem item = new DataItem { Name = "Z", Amount=1000, DOB=new DateTime(2014, 01, 01), Phone = "+12013452398" };
			TextWriter output;
			using(output = new StringWriter()) {
				var w = WriterFactory.Create(cols, output);
				w.Write(item);
				w.Write(data);
			}

			Assert.AreEqual(expected, output.ToString());
		}
	}
}

