﻿//
//  Copyright 2014  Alexander Samolov
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
using NUnit.Framework;
using System;

namespace CSVWriter.NUnit
{
	[TestFixture]
	public class ColumnsTest
	{
		internal static Columns<DataItem> CreateCols() {

			var cols = new Columns<DataItem>();
			cols.Add("Name", x => x.Name)
				.Add("Amount", x => x.Amount.ToString("N"))
				.Add("Date of Birth", x => x.DOB.ToString("dd-MM-yyyy"))
				.Add("Phone", x => x.Phone);

			return cols;
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullBothNameTest()
		{
			var cols = new Columns<DataItem>();
			cols.Add(null, null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullColumnFormatterTest()
		{
			var cols = new Columns<DataItem>();
			cols.Add("Name", null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullColumnNameTest()
		{
			var cols = new Columns<DataItem>();
			cols.Add(null, x => x.Name);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void EmptyColumnNameTest()
		{
			var cols = new Columns<DataItem>();
			cols.Add("", x => x.Name);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WhitespaceColumnNameTest()
		{
			var cols = new Columns<DataItem>();
			cols.Add("      ", x => x.Name);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullCopyConstructorTest()
		{
			var cols = new Columns<DataItem>(null);
			cols.Add("Name", x => x.Name);
		}

		[Test]
		public void CopyConstructorTest()
		{
			var cols1 = CreateCols();
			var cols = new Columns<DataItem>(cols1);

			Assert.AreEqual(cols.Count, cols1.Count, "Copy shall have the same amount of columns");

			var iter1 = cols1.GetEnumerator();
			var iter = cols.GetEnumerator();
			iter1.MoveNext();
			iter.MoveNext();
			for (int i = 0; i < cols.Count ; i++, iter1.MoveNext(), iter.MoveNext()) {
				var col1 = iter1.Current;
				var col = iter.Current;

				Assert.AreEqual(col1.Name, col.Name, "Names are different!");
				Assert.AreEqual(col1.Formatter, col.Formatter, "Formatters are different!");
			}

			cols.Add("AmountFormatted", x => x.Amount.ToString("C"));

			Assert.AreNotEqual(cols1.Count, cols.Count, "shall have different number of columns");
		}
	}
}

