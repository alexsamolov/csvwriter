# CSVWriter #

Simple writer which allows to dump objects as CSV. Library supports proper quotation of commas and double quotes. The set and content of output columns is highly customizable: you set the output format for each column you'd like to see in CSV file.

The CSV file is written to a stream, the stream lifecycle shall be managed in your program. 

### License ###

Copyright 2014  Alexander Samolov

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

### Examples ###

The usage is pretty simple:

1. Define the set of columns you'd like to see in your CSV.
1. For each column define serializer of an object.
1. Create the writer.
1. Write objects one-by-one or several items at once.

Example:

```cs
using CSVWriter;
//...

// Define the set of columns
var cols = new Columns<DataItem>();

// For each column define the name and serializer.
// The serializer shall return a string from the object.
cols.Add("Name", x => x.Name)
	.Add("Amount", x => x.Amount.ToString("N"))
	.Add("Date of Birth", x => x.DOB.ToString("dd-MM-yyyy"))
	.Add("Phone", x => x.Phone);

// Data objects
IEnumerable<DataItem> data = new DataItem[] {
	new DataItem { ... },
	new DataItem { ... }
};
DataItem item = new DataItem { ... };

TextWriter output;
using(output = new StringWriter()) {
	// Create the writer using WriterFactory and provide the columns descriptors and output stream
	var w = WriterFactory.Create(cols, output);

	// Write items, one-by-one or by whole collection.
	w.Write(item);
	w.Write(data);
}			
```
Also please see the unit tests for additional examples.

### Compilation ###

I used Xamarin Studio and Microsoft Visual Studio 2012 to develop and build the project. Please let me know if you'll have any problems compiling the library.

To run the tests you shall download the NuGet packages (nunit).

### Who do I talk to? ###

I hope you'll find this useful. In case of any questions please do not hesitate to contact me: Alexander Samolov (alexsamolov@gmail.com)